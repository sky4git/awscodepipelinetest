[![pipeline status](https://gitlab.com/sky4git/awscodepipelinetest/badges/master/pipeline.svg)](https://gitlab.com/sky4git/awscodepipelinetest/commits/master)


# AwsCodePipelineTest

AWS CodePipeline Test repository.

We are testing code upload to S3 bucket after testing finish to sample PHP code. Once code is uploaded to S3 bucket, AWS CodePipeline can capture that and go through its own process (that you can configure it in AWS CodePipeline)

# Deploy to AWS S3

Deployment process needs to configure aws-cli to upload the code zip file to S3 bucket. 
We are using three main ENVIRONMENT variables that is configured into this repository **securly**. 
1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY
3. S3_BUCKET  (bucket-name)


## Reference
* https://medium.com/@otbe/gitlab-ci-aws-codepipeline-a26d0a509a14
* https://docs.aws.amazon.com/cli/latest/userguide/installing.html
* https://docs.aws.amazon.com/codebuild/latest/userguide/build-spec-ref.html
* https://www.hostingmanual.net/zipping-unzipping-files-unix/
* https://www.freeformatter.com/cron-expression-generator-quartz.html
* https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html
* https://askubuntu.com/questions/795629/install-php-extensions-in-ubuntu-16-04